//
//  AppDelegate.h
//  EpicRun
//
//  Created by Admin on 12.08.13.
//  Copyright (c) 2013 Admin. All rights reserved.
//

#import <UIKit/UIKit.h>

@class SWRevealViewController;

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;
//@property (strong, nonatomic) SWRevealViewController *viewController;

@end
