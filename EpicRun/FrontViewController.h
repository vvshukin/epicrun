//
//  FrontViewController.h
//  EpicRun
//
//  Created by Admin on 8/7/13.
//  Copyright (c) 2013 Admin. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface FrontViewController : UIViewController

@property (nonatomic) IBOutlet UILabel *label;
@property (nonatomic) IBOutlet UINavigationBar *nav;

@property (nonatomic) NSString *text;

@end
