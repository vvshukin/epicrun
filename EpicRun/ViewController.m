//
//  ViewController.m
//  EpicRun
//
//  Created by Admin on 8/7/13.
//  Copyright (c) 2013 Admin. All rights reserved.
//

#import "ViewController.h"
#import "SWRevealViewController.h"
#import "FrontViewController.h"

@interface ViewController ()

@end

@implementation ViewController
{
    NSInteger _previouslySelectedRow;
}

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.clearsSelectionOnViewWillAppear = NO;
    self.tableView.backgroundColor = [UIColor colorWithWhite:0.3 alpha:1.0];
    self.tableView.separatorColor = [UIColor colorWithWhite:0.5 alpha:1.0];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    _previouslySelectedRow = -1;
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 3;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil)
    {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    }
    
    NSString *text = nil;
    switch ( indexPath.row )
    {
        case 0: text = @"Мои гонки"; break;
        case 1: text = @"Магазин"; break;
        case 2: text = @"Сканер"; break;
            
    }
    
    cell.imageView.image = [UIImage imageNamed:text];
    cell.imageView.frame = CGRectMake(0,0,44,44);
    cell.textLabel.text = text;
    cell.textLabel.textColor = [UIColor colorWithWhite:1.0 alpha:1.0];
    return cell;
}


- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    cell.backgroundColor = self.tableView.backgroundColor;
    
}

#pragma mark - Table view delegate





- (NSIndexPath *)tableView:(UITableView *)tableView willSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    return indexPath;
}



- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    SWRevealViewController *revealController = self.revealViewController;
    
    NSInteger row = indexPath.row;
    
    if ( row == _previouslySelectedRow )
    {
        [revealController revealToggleAnimated:YES];
        return;
    }
    
    _previouslySelectedRow = row;
    
    NSString *text = nil;
    switch ( row )
    {
        case 0: text = @"Мои гонки"; break;
        case 1: text = @"Магазин"; break;
        case 2: text = @"Сканер"; break;
            
    }
    UIViewController *frontController = nil;
    switch ( row )
    {
        case 0://кейсы на будущее
        case 1:
        case 2:
        {
            FrontViewController *labelController = [[FrontViewController alloc] init];
            labelController.text = text;
            frontController = labelController;
            break;
        }
            
    }
    
    [revealController setFrontViewController:frontController animated:NO];
    [revealController setFrontViewPosition:FrontViewPositionRight animated:YES];
    
}


@end
