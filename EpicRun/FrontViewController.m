//
//  FrontViewController.m
//  EpicRun
//
//  Created by Admin on 8/7/13.
//  Copyright (c) 2013 Admin. All rights reserved.
//

#import "FrontViewController.h"
#import "SWRevealViewController.h"

@interface FrontViewController ()
@property IBOutlet UIBarButtonItem* revealButtonItem;
@property (nonatomic, retain) IBOutlet UIView *calView;
@property IBOutlet UIBarButtonItem* revealButtonItemCal;

-(IBAction)displayNewView:(id)sender;
-(IBAction)displayOldView:(id)sender;


@end

@implementation FrontViewController
@synthesize calView;

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.label.text = _text;
    UIImage *backgroundImage = [UIImage imageNamed:@"3.png"];
    [self.nav setBackgroundImage:backgroundImage forBarMetrics:UIBarMetricsDefault];
    
    [self.revealButtonItem setTarget: self.revealViewController];
    [self.revealButtonItem setAction: @selector( revealToggle: )];
    [self.revealButtonItemCal setTarget: self.revealViewController];
    [self.revealButtonItemCal setAction: @selector( revealToggle: )];
    [self.nav addGestureRecognizer: self.revealViewController.panGestureRecognizer];
    [self.view addGestureRecognizer: self.revealViewController.panGestureRecognizer];
}

-(IBAction)displayNewView:(id)sender {
    UISegmentedControl *btn = sender;
    if (btn.selectedSegmentIndex == 1) {
        btn.selectedSegmentIndex=0;
        [[NSBundle mainBundle] loadNibNamed:@"CalView" owner:self options:nil];
        [self.view addSubview:calView];
    }
    
}


-(IBAction)displayOldView:(id)sender
{
    UISegmentedControl *btn = sender;
    if (btn.selectedSegmentIndex == 0) {
    [calView removeFromSuperview];
    }
}




@end
